#include "Joueur.h"

#ifndef _WIN32
//Tirage au sort du coup à jouer
std::random_device dev; //Connexion au random device de l'ordinateur
std::mt19937 rng(dev()); //Générateur de "hasard" basé sur le random device
#endif

int randint(int inf, int sup) {
    #ifdef _WIN32
        return rand() % (sup-inf+1) + inf;
    #else
        //On génère une distribution étalée qui garantit que tous les indices de loiProba ont autant de chances d'être tirés au sort
        //C'est un moyen de faire en pratique une loi de probabilité : telles que sont enregistrées les données, si un coup gagne 40%,
        //40% des "cases" de loiProba sont ce coup, donc il y a 40% de chances de choisir ce coup, c'est ce qu'on veut
        std::uniform_int_distribution<std::mt19937::result_type> dist(inf, sup);

        return dist(rng); //On récupère le coup tiré au "hasard" selon la loi de probabilité déterminée précédemment
    #endif
}

Joueur::Joueur(std::unordered_map<std::string, std::vector<int>>& situationsConnues, int id, std::vector<std::vector<int>> positionPieces, bool IA, bool apprentissageVolee) : m_id(id), m_estIA(IA), m_situationsConnues(situationsConnues)
{
    m_pieces = std::vector<Piece*>(0);
    m_pieces.reserve(5);

    for (std::vector<int> posPiece : positionPieces){
        //Cf constructeur de base au-dessus de celui-ci
        //posPiece[0] -> "position variable"
        //posPiece[1] -> "position fixe"
        //posPiece[2] -> sens de la pièce : 0 pour retour, 1 pour aller
        Piece* piece = new Piece(this, posPiece[0], posPiece[1], posPiece[2]);
        m_pieces.push_back(piece); //on ajoute la pièce à la liste de pièces
    }

    //Ce constructeur n'est généralement utilisé que pour l'apprentissage à la volée
    //Lors d'un apprentissage à la volée, l'ia n'a pas le droit de faire un apprentissage à la volée
    //Sinon c'est un cycle qui ne s'arrête que lorsque toutes les situations seront connues
    //Autant dire que c'est très très long
    m_apprentissageVolee = apprentissageVolee;
}
Joueur::~Joueur()
{
    for(int i=0; i < (int)m_pieces.size(); i++){
        delete m_pieces[i];
    }
}

void Joueur::coup(Plateau* plateau, int y)
{
    int positionVoulue = pieces()[y]->positionVoulueX(plateau);
    plateau->deplace(positionVoulue, pieces()[y], m_id);
}

int Joueur::id()
{
    return m_id;
}

bool Joueur::estIA()
{
    return m_estIA;
}

std::vector<Piece*>& Joueur::pieces()
{
    return m_pieces;
}

bool Joueur::gagnant()
{
    return (nbPiecesArrivees() >= 4); //Le joueur a gagné si 4 pièces sont arrivées
}

void Joueur::enregistreCoups()
{
    if (gagnant())
        for (std::pair<std::string, int> keyval : m_coups) {
            //keyval.first -> représentation textuelle de la situation
            //keyval.second -> le coup joué par le gagnant face à la situation
            if (!estDansMap(m_situationsConnues, keyval.first))
                m_situationsConnues[keyval.first] = {keyval.second};
            else
                m_situationsConnues[keyval.first].push_back(keyval.second);
        }
}

int Joueur::IA_coup(Plateau* plateau)
{
    /** Principe général de l'IA :
     * 1. Une unordered_map est utilisée pour stocker des "situations" (représentation textuelle d'une situation de plateau) et les coups associés
     * 2. On distinguera deux phases d'apprentissage distinctes : apprentissage en amont et à la volée.
     *    - L'apprentissage en amont désigne le lancement d'un certain nombre de parties de l'ia contre elle-même. Les données collectées (cf 3.)
     *    Il mène à un enregistrement des données dans un fichier .csv, qui peut être réutilisé pour les parties suivantes.
     *    - L'apprentissage à la volée désigne le fait d'utiliser la méthode d'apprentissage en amont en pleine partie.
     *    Il est nécessaire dans la mesure où il est impossible (avec la capacité de mémoire dont nous disposons) de faire un apprentissage en amont exhaustif
     *    Ainsi, au cours d'une partie, la grande majorité des situations sont inédites.
     *    L'idée est donc, lorsque l'on tombe sur une situation inédite lors d'une partie contre un autre joueur, de lancer un certain nombre de parties (ici 200),
     *    et de "voir" quel coup "marche" le mieux.
     *    Ces parties partent toutes de la nouvelle situation, permettant d'élaborer ex-nihilo une base pour choisir un coup pertinent.
     ** FONCTIONNEMENT PRATIQUE :
     *    Lors d'une partie (apprentissage en amont ou à la volée), les coups du gagnant sont enregistrés.
     *    Ainsi, pour une situation donnée, si le joueur gagnant (à la toute fin de la partie) a joué le coup "5", il est ajouté dans la unordered_map à l'index de la situation.
     *    Une situation peut être vue (et mener à une victoire) plusieurs fois, les coups gagnants sont ajoutés les uns après les autres.
     *    Formant ainsi une "loi de probabilité" : lors de l'apprentissage en amont, le coup est choisi selon cette loi de probabilité.
     *    Si 40% des victoires associées à une situation ont eu lieu après un coup "5", l'ia a 40% de jouer le coup "5" lorsqu'elle est confrontée à la situation.
     *    Lors d'une partie contre un autre joueur (lorsque l'apprentissage à la volée est autorisé), l'ia joue le coup le plus représenté dans la loi de probabilité.
     * 3. Note sur l'activation de l'apprentissage à la volée (m_apprentissageVolee) :
     *    - Lors d'un apprentissage en amont, il est désactivé, parce que pour chaque partie d'apprentissage en amont, il y aurait beaucoup plus de parties d'apprentissage à la volée.
     *    Une partie lambda faisant environ 40 échanges, s'il était activé, 2*40*200 = 16000 parties d'apprentissage à la volée seraient lancées par partie d'apprentissage en amont.
     *    - Lors d'une partie contre un joueur, il est activé MAIS lorsqu'il lance des parties d'apprentissage à la volée, celles-ci n'ont pas ce mode activé.
     *    Si les parties d'apprentissage à la volée avaient ce mode activé, ce serait un cycle très long de parties d'apprentissages dans des parties d'apprentissage...
     * Merci d'avoir pris le temps de lire !
    **/

    std::string situation = plateau2mini(plateau, this); //On calcule la représentation textuelle du plateau
    bool situationConnue = estDansMap(m_situationsConnues, situation);
    std::vector<int> loiProba;
    int coup;

    //Récupération de la loi de probabilité si la situation est connue
    //Si la situation n'est pas connue, on se contente d'une loi de probabilité de 0 élément (initialisée plus tard)
    loiProba = (situationConnue) ? m_situationsConnues[situation] : std::vector<int>(0);

    int coupsConnus = 0;
    if (situationConnue) coupsConnus = m_situationsConnues[situation].size();

    //Si la situation n'est pas connue, on fait un apprentissage à la volée
    //Plein de parties commençant par la situation actuelle sont jouées (pendant 3 secondes, 3000 millisecondes)
    //Et complètent la unordered_map d'apprentissage

    if (m_apprentissageVolee) {
        auto debut = std::chrono::high_resolution_clock::now();

        int nbParties = 0;
        for (nbParties = 0; std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - debut).count() < 3000; nbParties++) {
            Partie party = Partie(m_situationsConnues, situation);
            party.interaction();
        }

        std::cout << "Joueur IA n°" << m_id + 1 << std::endl;
        std::cout << "Nb parties : " << nbParties << std::endl;
        std::cout << "Pourcentage de victoires : " << (100.f*(m_situationsConnues[situation].size() - coupsConnus)) / nbParties << std::endl;

        loiProba = m_situationsConnues[situation];
    }

    //Si le joueur est autorisé à faire de l'apprentissage à la volée
    //(soit en pratique lorsqu'elle n'est pas en mode d'apprentissage tout court),
    //si la situation a un minimum de chances de mener à une victoire
    //On prend le meilleur coup (càd celui qui a mené au plus de victoires)
    //Sinon, on prend le coup qui mène au moins de défaites
    if (m_apprentissageVolee) {
        if (m_situationsConnues[situation].size() > 50) {
            coup = coupPlusFrequent(m_situationsConnues[situation]);

            std::cout << "Coup de l'ia : " << ((m_id) ? coup : 6-coup) << std::endl;
            m_coups[situation] = coup;

            return coup;
        } else
            std::cout << "Coup aléatoire…" << std::endl;
    }

    //On s'assure que tous les coups sont possibles (même avec une probabilité négligeable)
    //En apprentissage en amont, c'est pour s'assurer que l'IA essaie tous les coups, tôt ou tard -> apprentissage efficace
    //Lors d'une partie contre un joueur, ce cas ne se présente en pratique que si les chances de victoire de l'ia sont très proches de 0
    //Dans ce cas, autant jouer au hasard, ça pourrait déstabiliser l'adversaire !
    for (int i = 1; i <= 5; i++) {
        if (m_pieces[i-1]->arrive()) continue;

	      int nbNew = 5-std::count(loiProba.begin(), loiProba.end(), i);
      	for (int j = 0; j < nbNew; j++) loiProba.push_back(i);
    }

    coup = loiProba[randint(0, loiProba.size()-1)];

    m_coups[situation] = coup; //Enregistre le coup fait dans m_coups. Les coups du joueur seront enregistrés dans Joueur::enregistreCoups()
    return coup;
}

int Joueur::nbPiecesArrivees()
{
    int compteur = 0;
    for (Piece* piece : m_pieces)
        compteur += piece->arrive();

    return compteur;
}

std::unordered_map<std::string, std::vector<int>>& Joueur::bdSituations() {
    return m_situationsConnues;
}

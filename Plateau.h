#ifndef DEF_PLATEAU
#define DEF_PLATEAU

#include <vector>
#include <fstream>
#include <SFML/Graphics.hpp>

#include "primitives.h"

class Piece;
#include "Piece.h"
class Joueur;
#include "Joueur.h"

class Plateau
{
public:
    /** Constructeur de l'objet Plateau (faite par Paul)
     * @param un vector de pointeurs de joueurs, les joueurs de la partie
     * Complexité : O(1), un seule opération
     * Niveau de confiance dans l'implantation : très fort, sinon le jeu ne marcherait pas !
    **/
    Plateau(const std::vector<Joueur*>& joueurs);

    /** Fonction qui identifie de combien de cases peut avancer une pièce (faite par Fabrice)
     * @param le pointeur de la pièce
     * @return int le nombre en question
     * Complexité : O(1), le nombre d'opérations est constant
     * Niveau de confiance dans l'implantation : très fort, sinon le jeu ne marcherait pas !
    **/
    static int nbCasesDeplacement(Piece* piece);

    /** Fonction d'affichage du plateau dans la console, en fonction du joueur ayant le trait (faite par Fabrice)
     * @param le pointeur du joueur ayant le trait
     * Complexité : O(côté de la grille au carré)
     * Niveau de confiance dans l'implantation : fort, ça marche bien lors des parties en mode console
    **/
    void affichage(Joueur* jEnCours);

    /** Fonction d'affichage du plateau dans la fenêtre, en fonction du joueur ayant le trait (faite par Fabrice)
     * @param le pointeur de la fenêtre
     * @param le pointeur du joueur ayant le trait
     * Complexité : O(côté de la grille au carré)
     * Niveau de confiance dans l'implantation : très fort, sinon les problèmes d'affichage seraient choquants !
    **/
    void affichageFenetre(sf::RenderWindow* fenetre, Joueur* jEnCours);

    /** Fonction gérant le déplacement d'une pièce (prise de pièces adverses, arrêt prématuré, retournement...) (faite par Paul)
     * @param la position finale théorique de la pièce, sans prise de pièce adverse ni retournement
     * @param le pointeur de la pièce
     * @param l'id du propriétaire du joueur
     * Complexité : O(nombre de pièces)
     * Niveau de confiance dans l'implantation : très fort, les parties que nous avons faites nous ont donné la certitude que ça marche
    **/
    //-> nouvelle version
    void deplace(int posVoulue, Piece* pcsPosY, int idJoueur);

    /**Fonction vérifiant si un pion doit être mangé (faite par Paul)
     * @param la position voulue de notre pièce
     * @param les pièces à possiblement manger
     * @param l'indice de la pièce à partir de laquelle vérifier
     * @param le sens de la pièce
     * @return la position d'arrivée
     * Complexité : difficile à estimer, fonction récursive
     * Niveau de confiance dans l'implantation : très fort, cf deplace()
    **/
    //int verifDeplacement(int posArrive, std::vector<Piece*> pcsAdverse, int indice, Piece* pcsAlliee);

    /** Fonction qui renvoie le pointeur du joueur selon son id (fait par Fabrice)
     * @param l'id du joueur désiré
     * @return la référence du joueur correspondant
     * Complexité : O(1)
     * Niveau de confiance dans l'implantation : très fort, sinon plateau2mini() ne marcherait pas
    **/
    Joueur* joueurParId(int id);
private:
    std::vector<Joueur*> m_joueurs; //les joueurs de la partie
};

#endif

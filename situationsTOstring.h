#include <unordered_map>
#include <vector>
#include <fstream>
#include <sstream>
#include <limits>

class Plateau;
#include "Plateau.h"
class Joueur;
#include "Joueur.h"

/** Lecture du fichier CSV (situations.csv) avec les situations connues (faite par Fabrice)
 * @param la référence de la unordered_map dans laquelle enregistrer les données
 * Complexité : O(nombre de situations enregistrées)
 * Niveau de confiance dans l'implantation : très fort, lorsque l'ia a fait de l'apprentissage en amont, on constate que ça marche
**/
void lectureCSV(std::unordered_map<std::string, std::vector<int> >& situationsConnues);

/** Enregistrement dans le fichier CSV (situations.csv) avec les situations connues (fait par Fabrice)
 * @param la référence de la unordered_map dans laquelle sont les données à enregistrer
 * Complexité : O(nombre de situations dans la unordered_map)
 * Niveau de confiance dans l'implantation : très fort, le csv est bien enregistré en mode apprentissage en amont
**/
void enregistreCSV(std::unordered_map<std::string, std::vector<int> >& situationsConnues);

/** Fonction pour calculer la représentation textuelle réduite d'un plateau (uniquement la position des pièces) (faite par Fabrice)
 * @param le pointeur du plateau de jeu
 * @param le pointeur du joueur ayant le trait
 * @return la représentation textuelle réduite du plateau
 * Complexité : O(nombre de pièces)
 * Niveau de confiance dans l'implantation : très fort, sinon l'ia ne marcherait pas
**/
std::string plateau2mini(Plateau* plateau, Joueur* jEnCours);

/** Fonction qui extrait d'une représentation textuelle réduite de plateau la position des pièces correspondante (faite par Fabrice)
 * @param la représentation textuelle réduite du plateau
 * @return une std::pair qui contient d'un côté l'identifiant du joueur ayant le trait, et un std::vector avec les positions de toutes les pièces
 * Complexité : O(nombre de caractères dans mini) (chaque caractère désigne une pièce...)
 * Niveau de confiance dans l'implantation : très fort, sinon le jeu ne marcherait pas
**/
std::pair<int, std::vector<std::vector<std::vector<int>>>> mini2pieces(std::string mini);

/** Fonction qui détermine quel coup est le plus fréquent parmi un std::vector de coups (faite par Fabrice)
 * @param la référence du std::vector de coups
 * @return le coup le plus fréquent. En cas d'égalité, c'est le coup d'indice plus faible qui est renvoyé
 * Complexité : O(nombre d'éléments de coups)
 * Niveau de confiance dans l'implantation : très fort, sinon l'ia serait nettement moins bonne !
**/
int coupPlusFrequent(std::vector<int>& coups);

int coupMoinsFrequent(std::vector<int>& coups);

/** Fonction qui lit dans un fichier de configuration le lancement d'une partie (humain contre humain, contre IA...) (faite par Fabrice)
 * @return une paire avec ces infos
 * Complexité : O(1), le nombre d'opérations est constant
 * Niveau de confiance dans l'implantation : fort, quelques tests ont été faits à la main en modifiant le fichier de configuration
 *
 * Utilisation du fichier de configuration
 * Il s'appelle config.cfg. La première ligne décrit si le joueur 1 (horizontal) est humain ou ia. Il faut écrire exactement "ia" pour que le joueur soit
 * ia, sinon il est considéré comme humain. De même pour la deuxième ligne qui correspond au joueur adverse. La troisième ligne permet de choisir si
 * le jeu doit être lancé en console. Pour qu'il le soit, il faut écrire exactement "console", sinon le jeu se lance en mode fenêtré. La 4e ligne permet
 * de choisir si l'apprentissage à la volée est autorisé pour l'ia. Pour qu'il le soit, il faut écrire exactement "volee", sinon l'ia ne sera pas autorisée
 * à l'utiliser. La 5e ligne sert à préciser la situation de départ pour le jeu. Pour faire la situation de départ par défaut, il faut laisser cette ligne vide.
**/
std::tuple<bool, bool, bool, bool, std::string> infosPartie();

std::pair<int, std::vector<std::vector<std::vector<int>>>> lecteurEnregistrement();

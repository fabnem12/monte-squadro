#include "Plateau.h"
#include "Partie.h"
#include "Joueur.h"
#include "Piece.h"

Plateau::Plateau(const std::vector<Joueur*>& joueurs): m_joueurs(joueurs)
{
}

int Plateau::nbCasesDeplacement(Piece* piece)
{
    Joueur* proprio = piece->proprio(); //On identifie le joueur auquel appartient la pièce

    std::vector<int> deplacements;
    //La pièce appartient au premier joueur et elle est en phase d'aller OU elle appartient à l'autre joueur et elle est en phase de retour (cas similaires)
    if ((1-proprio->id() && piece->aller()) || (proprio->id() && !piece->aller()))
        deplacements = {1, 3, 2, 3, 1};
    else
        deplacements = {3, 1, 2, 1, 3};

    return deplacements[piece->positionFixe()-1]; //-1 parce que la position fixe va de 1 à 5, alors que les indices de ce vector vont de 0 à 4
}

void Plateau::affichage(Joueur* jEnCours)
{
    std::string affiPiece; //Le std::string affiché en console
    std::vector<Piece*> piecesJEnCours = jEnCours -> pieces();

    //On crée un tableau de string pour stocker les représentations des pièces en fonction de leur position sur le plateau
    std::vector<std::vector<std::string>> tableau = std::vector<std::vector<std::string>>(7, std::vector<std::string>(7)); //Déclaration, allocation

    for (Joueur* joueur : {m_joueurs[1], m_joueurs[0]}) {
        for (Piece* piece : joueur->pieces()) {
            //Détermine la position de la pièce dans le canvas en fonction de ses coordonnées
            //Les 6- sont dus au système de coordonnées des pièces assez ésotérique il faut l'admettre
            int posX = (1-joueur->id()) ? piece->positionVariable() : piece->positionFixe();
            int posY = (1-joueur->id()) ? 6-piece->positionFixe() : 6-piece->positionVariable();

            if (1-joueur->id()) {
                affiPiece = (piece->aller()) ? ">" : "<";
                affiPiece = "\033[1;32m" + affiPiece + "\033[0;0m"; //On affiche le caractère en vert
            } else {
                affiPiece = (piece->aller()) ? "^" : "V";
                affiPiece = "\033[1;31m" + affiPiece + "\033[0;0m"; //On affiche le caractère en rouge
            }

            tableau.at(posY).at(posX) = affiPiece; //Le caractère est mis dans le tableau d'affichage
        }
    }

    //Coins inaccessibles aux pièces représentés par des "x"
    tableau[0][0] = "x";
    tableau[0][6] = "x";
    tableau[6][0] = "x";
    tableau[6][6] = "x";

    for (int idLigne = 0; idLigne < 7; idLigne++) { //Les cases accessibles mais vides sont représentées par des "."
        for (int idColonne = 0; idColonne < 7; idColonne++) {
            if (tableau[idLigne][idColonne] == "")
                tableau[idLigne][idColonne] = ".";
        }
    }

    //Génère le "texte" à afficher en console
    std::string affi = "";
    affi += "- + - + - + - + - + - + - + -\n";
    for (int idLigne = 0; idLigne < 7; idLigne++) {
        affi += "| ";
        for (int idColonne = 0; idColonne < 7; idColonne++) {
            affi += tableau.at(idLigne).at(idColonne) + " | "; //Séparateur de cases
        }
        affi += "\n";
        affi += "- + - + - + - + - + - + - + -\n";
    }

    std::cout << affi;
}

void Plateau::affichageFenetre(sf::RenderWindow* fenetre, Joueur* jEnCours)
{
    float coteCase = 100; //100 pixels de côté par carré

    sf::Color couleurJ1 = sf::Color::Yellow; //Le joueur 1 (se déplaçant horizontalement) est représenté en jaune
    sf::Color couleurJ2 = sf::Color::Red; //L'autre en rouge

    sf::Texture textureMouton; //On récupère le dessin de mouton
    textureMouton.loadFromFile("graphics/mouton.png");
    sf::Image moutonRetour;
    moutonRetour.loadFromFile("graphics/mouton.png");
    sf::Image moutonAller;
    moutonAller.loadFromFile("graphics/mouton.png");
    moutonAller.flipHorizontally(); //On retourne le dessin pour faire l'aller et retour du mouton

    sf::Font font; //Typographie pour le texte
    font.loadFromFile("graphics/BrokenGlass.ttf");

    sf::Texture textureFond; //Fond de la grille "herbe", générée avec processing grâce au cours d'Info 112
    textureFond.loadFromFile("graphics/fond.png");
    sf::Sprite fond;
    fond.setTexture(textureFond);
    fond.setPosition(0, 0); //On dessine le fond
    fenetre->draw(fond);

    float posX; //Initialisation des variables de position des pièces
    float posY; //Float parce que SFML n'accepte que les float pour les positions...

    //Affichage des pièces
    for (Joueur* joueur : m_joueurs) {
        for (Piece* piece : joueur->pieces()) {
            //Détermine la position de la pièce dans le canvas en fonction de ses coordonnées
            posX = (1-joueur->id()) ? piece->positionVariable() : piece->positionFixe();
            posY = (1-joueur->id()) ? 6-piece->positionFixe() : 6-piece->positionVariable();

            //Pour centrer le triangle dans la case, vu que posX*coteCase est le bord gauche de la case !
            posX = posX * coteCase + coteCase/2;
            posY = posY * coteCase + coteCase/2;

            if (piece->aller())
                textureMouton.update(moutonAller);
            else
                textureMouton.update(moutonRetour);
            textureMouton.setSmooth(true);

            sf::Sprite mouton;
            mouton.setTexture(textureMouton);

            //Couleur du mouton selon le joueur
            mouton.setColor((1-joueur->id()) ? couleurJ1 : couleurJ2);
            if (piece->arrive()) //Si le mouton est arrivé, on le met en noir transparent
                mouton.setColor(sf::Color(0, 0, 0, 128));

            mouton.setOrigin(coteCase/2, coteCase/2);
            mouton.setPosition(posX, posY);

            if (1-joueur->id()) { //On recentre un peu le mouton dans sa case
                mouton.move(-coteCase/3, -coteCase/3);
            } else
                mouton.move(-coteCase/3, coteCase/3);

            mouton.setScale(0.25, 0.25); //L'image de mouton fait 380 pixels de côté, il faut la réduire pour tenir dans la case

            if (joueur->id()) //On fait une rotation pour le joueur vertical, pour que le mouton semble aller vers le haut/bas plutôt que vers la droite/gauche
                mouton.rotate(-90);

            //On affiche le nombre de cases de déplacements de chaque pièce
            sf::Text text;
            text.setFont(font);
            text.setFillColor(sf::Color::White);
            text.setString(std::to_string(nbCasesDeplacement(piece))); //setString n'accepte que les std::string
            text.setCharacterSize(30);
            text.setPosition((1-joueur->id()) ? (5*coteCase)/6 : posX-coteCase/2, (1-joueur->id()) ? posY+coteCase/6 : 7*coteCase - coteCase);

            //On met à l'écran le triangle représentant la pièce ainsi que le nombre de cases de déplacement
            fenetre->draw(mouton);
            //fenetre->draw(trianglePiece);
            fenetre->draw(text);
        }
    }

    //Affichage des coins
    //Coin sup gauche
    draw_line(*fenetre, {0, 0}, {coteCase, coteCase}, sf::Color::Black);
    draw_line(*fenetre, {0, coteCase}, {coteCase, 0}, sf::Color::Black);
    //Coin sup droite
    draw_line(*fenetre, {7*coteCase-coteCase, 0}, {7*coteCase, coteCase}, sf::Color::Black);
    draw_line(*fenetre, {7*coteCase-coteCase, coteCase}, {7*coteCase, 0}, sf::Color::Black);
    //Coin inf gauche
    draw_line(*fenetre, {0, 7*coteCase}, {coteCase, 7*coteCase-coteCase}, sf::Color::Black);
    draw_line(*fenetre, {0, 7*coteCase-coteCase}, {coteCase, 7*coteCase}, sf::Color::Black);
    //Coin inf droite
    draw_line(*fenetre, {7*coteCase-coteCase, 7*coteCase-coteCase}, {7*coteCase, 7*coteCase}, sf::Color::Black);
    draw_line(*fenetre, {7*coteCase-coteCase, 7*coteCase}, {7*coteCase, 7*coteCase-coteCase}, sf::Color::Black);

    //Affichage du quadrillage
    for (int i = 1; i <= 6; i++) {
        draw_line(*fenetre, {i*coteCase, 0}, {i*coteCase, 7*coteCase}, sf::Color::Black);
        draw_line(*fenetre, {0, i*coteCase}, {7*coteCase, i*coteCase}, sf::Color::Black);
    }

    //Affichage latéral
    //Joueur en cours
    draw_filled_rectangle(*fenetre, {700, 0}, 350, 100, (1-jEnCours->id()) ? sf::Color::Yellow : sf::Color::Red);

    std::string affichage = "Joueur " + std::to_string(jEnCours->id() + 1);
    if (jEnCours->estIA()) affichage += " (IA)";
    if (jEnCours->gagnant()) affichage += " a gagne !!!";

    sf::Text affiJEnCours;
    affiJEnCours.setFont(font);
    affiJEnCours.setString(affichage);
    affiJEnCours.setCharacterSize(50);
    affiJEnCours.setOrigin(0, 25);
    affiJEnCours.setPosition(700, 50);
    affiJEnCours.setFillColor(sf::Color::Black);
    fenetre->draw(affiJEnCours);

    //Résumé des pièces arrivées
    sf::Sprite mouton;
    for (int idJoueur = 0; idJoueur < 2; idJoueur++) {
        draw_filled_rectangle(*fenetre, {700, 100 + idJoueur*100.f}, 350, 100, (1-idJoueur) ? sf::Color::Yellow : sf::Color::Red);

        for (int i = 0; i < 4; i++) {
            mouton.setTexture(textureMouton);
            mouton.setColor((m_joueurs[idJoueur]->nbPiecesArrivees() > i) ? sf::Color::White : sf::Color::Black);
            mouton.setPosition(700 + i*87, 100 + idJoueur*100.f);

            mouton.setScale(0.25, 0.25);

            fenetre->draw(mouton);
        }
    }

    draw_line(*fenetre, {700, 100}, {1050, 100}, sf::Color::Black); //Ligne de séparation entre l'affichage du joueur en cours et le résumé des pièces arrivées

    //Photo Monte Carlo
    sf::Texture textureMonte;
    textureMonte.loadFromFile("graphics/monte_carlo.jpg");

    sf::Sprite monteCarlo;
    monteCarlo.setTexture(textureMonte);
    monteCarlo.setPosition(700, 300);
    fenetre->draw(monteCarlo); //On affiche la photo du casino de monte carlo

    sf::Font fontMonte;
    fontMonte.loadFromFile("graphics/Tuffy.ttf");

    sf::Text textMonte;
    textMonte.setString("Monte Carlo powered AI");
    textMonte.setPosition(700, 300+232); //L'image de monte carlo fait 232 pixels de haut
    textMonte.setFont(fontMonte);
    textMonte.setCharacterSize(30);
    textMonte.setFillColor(sf::Color::Black);
    fenetre->draw(textMonte);

    //Logo squadro
    sf::Texture textureLogo;
    textureLogo.loadFromFile("graphics/logo_squadro.png");

    sf::Sprite logo;
    logo.setTexture(textureLogo);
    logo.setPosition(700, 700-103); //L'image fait 103 pixels de haut
    fenetre->draw(logo);

    fenetre->display();
}

void Plateau::deplace(int posVoulue, Piece* pPiece, int idJoueur) {
    std::vector<Piece*> piecesAdversesPertinentes(0);
    piecesAdversesPertinentes.reserve(5);

    for (Piece* piece : m_joueurs[1-idJoueur]->pieces())
        if (piece->positionVariable() == pPiece->positionFixe())
            piecesAdversesPertinentes.push_back(piece);

    int casesRestantes = abs(posVoulue - pPiece->positionVariable());
    bool aMange = false;
    int increment = (pPiece->aller()) ? 1 : -1;

    while (casesRestantes > 0) {
        std::vector<Piece*> piecesBloquantes(0);
        piecesBloquantes.reserve(5);

        for (Piece* piece : piecesAdversesPertinentes)
            if (piece->positionFixe() == pPiece->positionVariable() + increment)
                piecesBloquantes.push_back(piece);

        pPiece->setPosVariable(pPiece->positionVariable() + increment);
        if (!piecesBloquantes.size()) {
            if (aMange) casesRestantes = 0;

            if ((pPiece->positionVariable() == 6 && pPiece->aller()) || pPiece->positionVariable() == 0) {
                if (pPiece->aller()) pPiece->setAller();
                casesRestantes = 0;
            } else
                casesRestantes -= 1;
        } else {
            piecesBloquantes[0]->retourDepart();
            aMange = true;
        }
    }
}

/**
void Plateau::deplace(int posVoulue, Piece* pcsPosY, int idJoueur)
{
    Joueur* jAdverse = m_joueurs[1-idJoueur];
    std::vector<Piece*> pAdverses = jAdverse->pieces();

    for(unsigned int i=0; i<pAdverses.size(); i++){  //On vérifie toutes positions des pièces, solution temporaire
        if(abs(posVoulue - pcsPosY->positionVariable()) == 3 && pAdverses[i]->positionFixe() >= pcsPosY->positionVariable() && pAdverses[i]->positionFixe() == pcsPosY->positionVariable()+1 && pcsPosY->aller() && pAdverses[i]->positionVariable() == pcsPosY->positionFixe())
            posVoulue = pcsPosY->positionVariable() + 2;
        else if (abs(posVoulue - pcsPosY->positionVariable()) == 3 && pAdverses[i]->positionFixe() <= pcsPosY->positionVariable() && pAdverses[i]->positionFixe() == pcsPosY->positionVariable()+1 && !pcsPosY->aller() && pAdverses[i]->positionVariable() == pcsPosY->positionFixe()){
            posVoulue = pcsPosY->positionVariable() - 2;
        }
    }
    for(unsigned int i=0; i<pAdverses.size(); i++){  //On vérifie toutes positions des pièces, solution temporaire
        //std::cout << std::endl;
        //std::cout << "posVoulue - pcsPosY->positionVariable()" << (posVoulue - pcsPosY->positionVariable()) << std::endl;

        //std::cout << "posVoulue base:" << posVoulue << std::endl;

        posVoulue = verifDeplacement(posVoulue, pAdverses, i, pcsPosY);
    }

    if(posVoulue >= 6){ //On vérifie que la positionVoulue ne sorte pas du plateau à l'aller
        pcsPosY->setAller(); // On change le sens de la pièce (elle passe de aller à retour)
        pcsPosY->setPosVariable(6);
    }
    else if(posVoulue < 0){ //Vérification que la positionVoulue ne sorte pas du plateau au retour
        pcsPosY->setPosVariable(0);
    }
    else if(posVoulue < 6){ //Si la position voulue est inférieur à 6, la pièce prend cette position
        pcsPosY->setPosVariable(posVoulue);
    }
}
int Plateau::verifDeplacement(int posArrive, std::vector<Piece*> pcsAdverse, int indiceInt, Piece* pcsAlliee)
{
    unsigned int indice = indiceInt;
    if (indiceInt == -1) indice = 0;
    bool sens = pcsAlliee->aller();
    // On vérifie que les conditions sont réunis pour manger 1 pion à l'aller
    if (pcsAdverse[indice]->positionFixe() >= pcsAlliee->positionVariable() && pcsAdverse[indice]->positionFixe() <= posArrive && sens && pcsAdverse[indice]->positionVariable() == pcsAlliee->positionFixe()){
        posArrive = (pcsAdverse[indice]->positionFixe() == posArrive) ? posArrive + 1 : posArrive; //On augmente la position d'arrivée de 1 si on trouve une pièce Adverse à la position d'arrivée
        pcsAdverse[indice]->setPosVariable((pcsAdverse[indice]->aller()) ? 0 : 6); //Si la pièce adverse est au retour, sa position de renvoi est la 6ème case, sinon c'est la case de base
        if(indice+1 >= pcsAdverse.size()){ //On évite de sortir des limites du vector des pièces Adverses (entre 0 et 4)
                return posArrive;
        }
        posArrive = verifDeplacement(posArrive, pcsAdverse, indice+1, pcsAlliee); // On utilise la récursivité pour vérifier chaque pièce après la notre
    }

    if(pcsAdverse[indice]->positionFixe() <= pcsAlliee->positionVariable() && pcsAdverse[indice]->positionFixe() >= posArrive && !sens && pcsAdverse[indice]->positionVariable() == pcsAlliee->positionFixe()){
        posArrive = (pcsAdverse[indice]->positionFixe() == posArrive) ? posArrive - 1 : posArrive;

        pcsAdverse[indice]->setPosVariable((pcsAdverse[indice]->aller()) ? 0 : 6);
        if(indice-1 < 0){ //On évite de sortir des limites du vector des pièces Adverses (entre 0 et 4)
            return posArrive;
        }
        posArrive = verifDeplacement(posArrive, pcsAdverse, indice-1, pcsAlliee);
    }
    return posArrive;
}**/



Joueur* Plateau::joueurParId(int id)
{
    return m_joueurs[id];
}

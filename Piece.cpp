#include "Piece.h"

Piece::Piece(Joueur* proprio, int posVariable, int posFixe, bool aller)
{
    m_positions = std::vector<int>(2);
    m_positions[0] = posVariable;
    m_positions[1] = posFixe;
    m_proprio = proprio;
    m_aller = aller;
}

bool Piece::aller()
{
    return m_aller;
}

bool Piece::arrive()
{
    //La pièce est arrivée si elle a une position variable à 0 (position de départ) et qu'elle n'est pas en sens aller
    return (m_positions[0] == 0 && !m_aller);
}

int Piece::positionVariable()
{
    return m_positions[0]; //La position variable est le premier indice de m_positions (cf constructeur)
}

int Piece::positionFixe()
{
    return m_positions[1]; //La position fixe est le deuxième indice de m_positions (cf constructeur)
}

void Piece::setPosVariable(int posX)
{
    m_positions[0] = posX;
}

void Piece::setAller()
{
    m_aller = !m_aller;
}

int Piece::positionVoulueX(Plateau* plateau)
{
    int posVoulue(0);
    if(m_aller == true)
        posVoulue = positionVariable() + plateau->nbCasesDeplacement(this);
    else
        posVoulue = positionVariable() - plateau->nbCasesDeplacement(this);
    return posVoulue;
}

Joueur* Piece::proprio()
{
    return m_proprio;
}

void Piece::retourDepart() {
    setPosVariable((m_aller) ? 0 : 6);
}

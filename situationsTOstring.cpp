#include "situationsTOstring.h"

//Caractères utilisés pour représenter une pièce (selon sa position et son sens)
std::vector<char> pieces2Mini({'\a','!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y','z', '{', '|', '}', '~', ' ','\t'});

void lectureCSV(std::unordered_map<std::string, std::vector<int> >& situationsConnues)
{
    std::ifstream csv;
    csv.open("situations.csv");

    std::string situation;
    std::string coupsGagnantsStr;
    std::vector<int> coupsGagnants;

    std::string ligne;
    std::string receptacle;
    while (std::getline(csv, ligne)) { //Séparation par des points-virgules
        coupsGagnants = std::vector<int>(0); //On réinitialise la liste des coups gagnants
        std::stringstream ligneSS(ligne);

        std::getline(ligneSS, receptacle, ';');
        situation = receptacle;
        std::getline(ligneSS, receptacle, ';');
        coupsGagnantsStr = receptacle;

        for (char coup : coupsGagnantsStr)
            coupsGagnants.push_back(coup - '0'); //Astuce pour convertir un char en int

        situationsConnues[situation] = coupsGagnants;
    }

    csv.close();
}

void enregistreCSV(std::unordered_map<std::string, std::vector<int> >& situationsConnues)
{
    std::ofstream csv;
    csv.open("situations.csv");
    std::string loiProba;

    for (std::pair<std::string, std::vector<int> > keyval : situationsConnues) {
        loiProba = "";
        for (int coup : keyval.second)
            loiProba += std::to_string(coup);

        csv << keyval.first << ";" << loiProba << "\n";
    }

    csv.close();
}

std::pair<int, std::vector<std::vector<std::vector<int>>>> mini2pieces(std::string mini)
{
    int idJoueurTrait = mini[0] - '0';
    mini = mini.substr(1, mini.size() - 1);
    std::vector<std::vector<std::vector<int>>> pieces = std::vector<std::vector<std::vector<int>>>(2, std::vector<std::vector<int>>(0));
    std::pair<int, std::vector<std::vector<std::vector<int>>>> infosPieces;

    int posX; int posY;

    int i = 0;
    for (char pieceStr : mini) {
        int idSubstitution = -1;
        for (int index = 0; index < (int)pieces2Mini.size(); index++) {
            if (pieces2Mini[index] == pieceStr) idSubstitution = index;
        }

        int aller = (idSubstitution < 48);
        idSubstitution %= 48;

        posY = idSubstitution / 7;
        //idSubstitution /= 7;
        posX = idSubstitution % 7;

        std::vector<int> piece = std::vector<int>(3);
        //piece = (i/5 == 0) ? {posY, posX, aller} : {posX, posY, aller};
        piece[0] = (i/5) ? 6-posY : posX; //Pos variable
        piece[1] = (i/5) ? posX : 6-posY; //Pos fixe
        piece[2] = aller;

        pieces[i/5].push_back(piece);

        i++;
    }

    infosPieces = {idJoueurTrait, pieces};

    return infosPieces;
}

std::string plateau2mini(Plateau* plateau, Joueur* jEnCours)
{
    std::string mini = std::to_string(jEnCours->id()); //Le premier caractère de la std::string est l'identifiant du joueur ayant le trait

    int posX(0);
    int posY(0);
    int idSubstitution; //L'index du caractère de substitution utilisé

    for (Joueur* joueur : {plateau->joueurParId(0), plateau->joueurParId(1)}) { //On enregistre d'abord les pièces du joueur 0 puis du joueur 1
        for (Piece* piece : joueur->pieces()) {
            //Les 6- servent à compenser le système de coordonnées des pièces, assez ésotérique il est vrai
            posX = (1-joueur->id()) ? piece->positionVariable() : piece->positionFixe();
            posY = (1-joueur->id()) ? 6-piece->positionFixe() : 6-piece->positionVariable();

            idSubstitution = posY * 7 + posX; //On enregistre la position linéaire de la pièce
            if (!piece->aller()) idSubstitution += 48; //Telle qu'idSubstitution est calculée, elle vaut au maximum 48 avant cette étape
            mini += pieces2Mini[idSubstitution]; //On ajoute le caractère correspondant à la pièce à mini
        }
    }

    return mini;
}

int coupPlusFrequent(std::vector<int>& coups)
{
    std::vector<int> nbOccurences = std::vector<int>(5, 0);

    for (int coup : coups) nbOccurences[coup-1]++;

    int max = -1;
    int maxOccurences = -1;
    for (int i = 0; i < 5; i++) {
        if (nbOccurences[i] > maxOccurences) {
            maxOccurences = nbOccurences[i];
            max = i+1;
        }
    }

    return max;
}

int coupMoinsFrequent(std::vector<int>& coups)
{
    std::vector<int> nbOccurences = std::vector<int>(5, 0);

    for (int coup : coups) nbOccurences[coup-1]++;

    int min = 10000;
    double minOccurences = std::numeric_limits<double>::infinity();
    for (int i = 0; i < 5; i++) {
        if (nbOccurences[i] == (int)coups.size()) return i+1; //Si tous les coups sont le même nb, on prend celui-là sans hésiter

        if (nbOccurences[i] < minOccurences && nbOccurences[i] != 0) {
            minOccurences = nbOccurences[i];
            min = i+1;
        }
    }

    return min;
}

std::tuple<bool, bool, bool, bool, std::string> infosPartie()
{
    std::ifstream fichierConfig;
    fichierConfig.open("config.cfg");
    std::vector<std::string> lignes = {"human", "human", "fenetre", "volee", "0D=5.'LMNOP"};

    std::string ligne;
    for (int idLigne = 0; getline(fichierConfig, ligne) && idLigne < 5; idLigne++) {
        if (idLigne == 4 && ligne.size() != 11) //On essaie de ne pas prendre en compte une représentation du plateau erronée
            continue;

        lignes[idLigne] = ligne;
    }

    fichierConfig.close();

    std::tuple<bool, bool, bool, bool, std::string> infos = make_tuple((lignes[0] == "IA" || lignes[0] == "ia"), (lignes[1] == "IA" || lignes[1] == "ia"), (lignes[2] == "console"), (lignes[3] == "volee"), lignes[4]);

    return infos;
}

std::pair<int, std::vector<std::vector<std::vector<int>>>> lecteurEnregistrement()
{
    std::ifstream fichier("etat_jeu.txt");
    /*Ce tableau représente toutes les pièces du jeu, avec par rapport aux autres fonctions,
    une particularité: Il possède aussi l'identifiant du Joueur (0 si J1, 1 si J2)*/
    std::vector<std::vector<int>> positionsHor = std::vector<std::vector<int>>(5, std::vector<int>(0));
    std::vector<std::vector<int>> positionsVer = std::vector<std::vector<int>>(5, std::vector<int>(0));
    std::vector<std::string> lignes;
    std::string ligne("");
    std::string ligne1("");
    int posLigne(0), joueurHor(0);
    /*On lit la première ligne, deux avantages:
        - On passe la ligne ce qui évite de fausser les résultats de la boucle while
        - On récupère le deplacement par défaut du premier pion horizontal, ce qui nous donne, in fine, l'identifiant du joueur horizontal
    */
    getline(fichier, ligne1);
    while(getline(fichier, ligne)){
        lignes.push_back(ligne); //On crée le vector image du tableau dans le .txt
        //std::cout << getline(fichier, ligne) << std::endl;
        for(int posColonne = 0; posColonne < (int)ligne.size(); posColonne += 2){
            // Nous ne connaissons pas l'id du joueur, nous le supposons pour l'instant
            if(ligne[posColonne] == 'V'){ //retour
                positionsVer.at(posColonne / 2 - 1) = {6-posLigne, posColonne / 2, 0};
            }
            else if(ligne[posColonne] == '^'){ //aller
                positionsVer.at(posColonne / 2 - 1) = {6-posLigne, posColonne / 2, 1};
            }
            else if(ligne[posColonne] == '>'){ //aller
                positionsHor.at(posLigne - 1) = {posColonne / 2, posLigne, 1};
            }
            else if(ligne[posColonne] == '<'){ //retour
                positionsHor.at(posLigne - 1) = {posColonne / 2, posLigne, 0};
                std::cout << "< détecté : " << posColonne / 2 << " " << posLigne << " 0" << std::endl;
            }
        }
        posLigne++;
    }
    fichier.close();

    if (ligne1[0] == '1') joueurHor = 0;
    else joueurHor = 1;

    std::vector<std::vector<int>> piecesJoueur0 = (1-joueurHor) ? positionsHor : positionsVer;
    std::vector<std::vector<int>> piecesJoueur1 = (1-joueurHor) ? positionsVer : positionsHor;

    return {joueurHor, {piecesJoueur0, piecesJoueur1}};
}

#ifndef DEF_PARTIE
#define DEF_PARTIE

#include <fstream>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <unordered_map>

#include "situationsTOstring.h"

class Joueur;
#include "Joueur.h"

class Partie
{
public:
    ~Partie(); //Destructeur

    /** Constructeur (fait par Fabrice)
     * @param la référence de la unordered_map recensant les situations connues
     * @param la représentation textuelle de la situation de plateau en cours (qui inclut le joueur ayant le trait)
     * @param le jeu est-il en mode console ? (sinon, il est en mode fentré)
     * @param le jeu est-il en mode apprentissage ? (ce qui retire l'interaction)
     * @param le joueur horizontal est-il une IA ?
     * @param le joueur vertical est-il une IA ?
     * @param l'apprentissage à la volée est-il autorisé pour les IA ?
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    Partie(std::unordered_map<std::string, std::vector<int>>& situationsConnues, std::string mini, bool console = true, bool apprentissage = true, bool IA1 = true, bool IA2 = true, bool apprentissageVolee = false);

    /** Constructeur (fait par Fabrice)
     * @param la référence de la unordered_map recensant les situations connues
     * @param la std::pair décrivant les positions et sens des pièces de chaque joueur
     * @param le jeu est-il en mode console ? (sinon, il est en mode fenêtré)
     * @param le jeu est-il en mode apprentissage ? (ce qui retire l'interaction)
     * @param le joueur horizontal est-il une IA ?
     * @param le joueur vertical est-il une IA ?
     * @param l'apprentissage à la volée est-il autorisé pour les IA ?
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    Partie(std::unordered_map<std::string, std::vector<int>>& situationsConnues, std::pair<int, std::vector<std::vector<std::vector<int>>>> infosPieces, bool console = true, bool apprentissage = true, bool IA1 = true, bool IA2 = true, bool apprentissageVolee = false);

    /** Fonction gérant la communication avec l'utilisateur (demande des coups, affichage du plateau, basculement d'un joueur à l'autre, détection d'un gagnant) (faite par Fabrice et Paul)
     * Complexité : O(nombre de coups joués lors de la partie)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    void interaction();

    /** Fonction demandant l'affichage au Plateau (selon le mode : console ou fenêtre) (faite par Fabrice)
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    void affichePlateau();

    /** Fonction gérant le changement de Joueur (faite par Paul)
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    void changementJoueur();

    /** Demander au joueur son coup, selon le mode d'intéraction (faite par Fabrice)
     * @return l'id ("position fixe") de la pièce que le joueur souhaite bouger (entre 0 et 4)
     * Complexité : O(nombre de tentatives de jouer) (la fonction ne s'arrête que si un coup valide est entré)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    int demandeCoup();

    /** Demande la confirmation de fin de coup (faite par Fabrice)
     * Complexité : O(1)
     * Confiance dans l'implantation : les nombreuses parties en mode console nous ont montré que cette fonction fonctionne comme voulu
    **/
    void finCoup();

    /** Fonction gérant la fin de partie (faite par Paul)
     * @return un booléen valant vrai si la partie est terminée
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon la fin de partie ne marcherait pas !
    **/
    bool finPartie();

    /** Fonction vérifiant que le coup est valide (faite par Paul)
     * @param y l'id ("position fixe") de la pièce que l'on joue
     * @return true si le coup est valide, false sinon
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    bool verifCoup(int coup);

    /** Fonction qui modifie le fichier de configuration afin de pouvoir commencer une partie par la situation en cours (faite par Fabrice)
     * Si un joueur a gagné, on fait en sorte que la prochaine partie commence de nouveau sur la situation de départ.
     * Sinon, la prochaine partie démarre sur la dernière situation observée.
     * Complexité : O(1)
     * Niveau de confiance dans l'implantation : fort
    **/
    void enregistrement();

    /** Accesseur de plateau
     * @return le pointeur du plateau
    **/
    Plateau* plateau();

    /** Accesseur du joueur en cours
     * @return le pointeur du joueur en cours
    **/
    Joueur* joueurEnCours();

    /** Accesseur de m_joueurs
     * @return m_joueurs
    **/
    std::vector<Joueur*>& joueurs();
private:
    bool m_modeConsole;
    int m_idJoueur;
    std::vector<Joueur*> m_joueurs;
    Plateau* m_gameBoard;
    sf::RenderWindow* m_fenetre;
    bool m_apprentissage;
};

#endif // DEF_PARTIE

// Modules pour le jeu, en console uniquement pour le moment
#include <unordered_map>
#include <vector>

#include "situationsTOstring.h"

// Classes pour le jeu
#include "Partie.h"
#include "Joueur.h"
#include "Plateau.h"
#include "Piece.h"

/** Infrastructure minimale de test **/
#define ASSERT(test) if (!(test)) std::cout << "Test failed in file " << __FILE__ << " line " << __LINE__ << ": " #test << std::endl

void tests(std::unordered_map<std::string, std::vector<int>>& situationsConnues) {
    //Test coupPlusFrequent
    std::vector<int> vecIntTest;
    vecIntTest = {1, 2, 3, 4, 5, 1};
    ASSERT(coupPlusFrequent(vecIntTest) == 1);
    vecIntTest = {2, 2, 2, 2, 2, 2};
    ASSERT(coupPlusFrequent(vecIntTest) == 2);
    vecIntTest = {1, 2, 3, 4, 5, 3};
    ASSERT(coupPlusFrequent(vecIntTest) == 3);

    //Test mini2pieces (dans situationsTOstring.cpp)
    std::pair<int, std::vector<std::vector<std::vector<int>>>> compare = {0, {{{0, 1, 1}, {0, 2, 1}, {0, 3, 1}, {0, 4, 1}, {0, 5, 1}}, {{0, 1, 1}, {0, 2, 1}, {0, 3, 1}, {0, 4, 1}, {0, 5, 1}}}};
    ASSERT(mini2pieces("0D=5.'LMNOP") == compare);

    //Test plateau2mini (dans situationsTOstring.cpp)
    Partie party = Partie(situationsConnues, "0D=5.'LMNOP", true, true, true, true, true);
    ASSERT(plateau2mini(party.plateau(), party.joueurEnCours()) == "0D=5.'LMNOP");

    //Test Joueur::gagnant et Joueur::nbPiecesArrivees
    Partie party2 = Partie(situationsConnues, "0Dmf_X|}~jP", true, true, true, true, true);
    ASSERT(party2.joueurEnCours()->nbPiecesArrivees() == 4);
    ASSERT(party2.joueurEnCours()->gagnant());

    //Test Piece::arrive
    ASSERT(party2.joueurEnCours()->pieces()[1]->arrive());
    ASSERT(party2.joueurEnCours()->pieces()[2]->arrive());
    ASSERT(!party2.joueurEnCours()->pieces()[0]->arrive());

    //Test Piece::posFixe
    ASSERT(party2.joueurEnCours()->pieces()[0]->positionFixe() == 1);
    ASSERT(party2.joueurEnCours()->pieces()[1]->positionFixe() == 2);

    //Test Piece::posVariable
    ASSERT(party2.joueurEnCours()->pieces()[1]->positionVariable() == 0);
    ASSERT(party2.joueurEnCours()->pieces()[0]->positionVariable() == 0);

    //Test Piece::positionVoulueX
    ASSERT(party2.joueurEnCours()->pieces()[0]->positionVoulueX(party2.plateau()) == 1);

    //Test Piece::proprio
    ASSERT(party2.joueurEnCours()->pieces()[0]->proprio() == party2.joueurEnCours());
    ASSERT(party2.joueurEnCours()->pieces()[4]->proprio() == party2.joueurEnCours());

    //Test Plateau::nbCasesDeplacement
    ASSERT(party2.plateau()->nbCasesDeplacement(party2.joueurEnCours()->pieces()[4]) == 3);
    ASSERT(party2.plateau()->nbCasesDeplacement(party2.joueurEnCours()->pieces()[0]) == 1);
}

int main(int argc, char* argv[]) {
    #ifdef _WIN32
    srand(time(0));
    #endif

    bool apprentissage = (argc > 1); //On passe en mode apprentissage si le jeu est lancé avec un argument (le nb de parties d'apprentissage en amont)

    std::unordered_map<std::string, std::vector<int>> situationsConnues; //Les situations connues, utilisées pour l'ia
    lectureCSV(situationsConnues);

    //On lance les tests avant la partie pour s'assurer que tout marche bien
    tests(situationsConnues);

    int nbParties = 1;
    if (apprentissage)
        nbParties = std::stoi(argv[1]); //On convertit le nombre de parties passé en argument en entier utilisable

    //On récupère la configuration de jeu
    std::tuple<bool, bool, bool, bool, std::string> infos = infosPartie();
    bool IA1 = std::get<0>(infos);
    bool IA2 = std::get<1>(infos);
    bool console = std::get<2>(infos);
    bool apprentissageVolee = (!apprentissage && std::get<3>(infos));
    std::string situation = std::get<4>(infos); //infosPartie() fait en sorte que par défaut "situation" correspond à la situation de départ du jeu

    if (apprentissage) {
      IA1 = IA2 = true;
      console = true;
    }
    if (apprentissage && std::get<3>(infos)) {
        std::cout << "Voulez-vous VRAIMENT faire de l'aprentissage avec apprentissage à la volée ? (oui ou non)" << std::endl;
        std::string reponse;
        std::cin >> reponse;
        apprentissageVolee = (reponse == "oui");
    }

    //On voit si le coach est activé en mode automatique ou pas
    if ((IA1 && !IA2) || (!IA1 && IA2)) {
        std::ifstream configCoach;
        configCoach.open("coachAuto.txt");
        std::string param;
        configCoach >> param;
        bool coachAuto = (param == "oui");
        if (coachAuto && nbParties > 1) console = true;
        if (coachAuto && IA1)
            IA2 = false;
        else if (coachAuto && IA2)
            IA1 = false;

        apprentissage = false;
    }

    //On lance autant de parties que voulu (pour l'apprentissage en amont)
    for (int idPartie = 0; idPartie < nbParties; idPartie++) {
        if (apprentissage) {
            if ((idPartie+1) % 1000 == 0) std::cout << "Partie d'apprentissage n°" << idPartie+1 << std::endl;

            Partie party = Partie(situationsConnues, situation, true, true, true, true, apprentissageVolee);
            party.interaction();
        } else {
            Partie party = Partie(situationsConnues, situation, console, apprentissage, IA1, IA2, apprentissageVolee);
            party.interaction();
            situationsConnues.clear();
        }
    }

    if (apprentissage) { //On enregistre dans un csv le résultat de l'apprentissage en amont s'il a eu lieu
        std::cout << situationsConnues.size() << " situations connues desormais" << std::endl;
        enregistreCSV(situationsConnues);
    }

    return 0;
}

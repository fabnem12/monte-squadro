#include <stdexcept>
#include "primitives.h"
#include <assert.h>
#include <cmath>
#include <iostream>

void draw_point(sf::RenderWindow& w, Point pos, sf::Color color) {
    sf::Vertex p[] = { sf::Vertex(pos, color) };
    w.draw(p, 1, sf::Points);
}

void draw_line(sf::RenderWindow& w, Point pos1, Point pos2, sf::Color color) {
    sf::Vertex p[] = { sf::Vertex(pos1, color), sf::Vertex(pos2, color) };
    w.draw(p, 2, sf::Lines);
}

void draw_circle(sf::RenderWindow& w, Point center, int r, sf::Color color) {
    sf::CircleShape shape(r);
    shape.setPosition(center);
    shape.setOutlineThickness(1.f);
    shape.setOutlineColor(color);
    shape.setFillColor(sf::Color::Transparent);
    w.draw(shape);
}

void draw_filled_circle(sf::RenderWindow& w, Point center, int r, sf::Color color) {
    sf::CircleShape shape(r);
    shape.setPosition(center);
    shape.setOutlineThickness(0.f);
    shape.setFillColor(color);
    w.draw(shape);
}

void draw_rectangle(sf::RenderWindow& w, Point pos, int width, int height, sf::Color color) {
    sf::RectangleShape shape(sf::Vector2f(width, height));
    shape.setPosition(pos); //Top-left position
    shape.setOutlineThickness(1.f);
    shape.setOutlineColor(color);
    shape.setFillColor(sf::Color::Transparent);
    w.draw(shape);
}

void draw_filled_rectangle(sf::RenderWindow& w, Point pos, int width, int height, sf::Color color) {
    sf::RectangleShape shape(sf::Vector2f(width, height));
    shape.setPosition(pos); //Top-left position
    shape.setOutlineThickness(1.f);
    shape.setOutlineColor(color);
    shape.setFillColor(color);
    w.draw(shape);
}

sf::Font minipax;
bool loadfont() {
    static bool loaded = false;
    /**
     * Using the Minipax font, by Raphaël Ronot
     * Under the SIL Open Font License
     * Retrieved on Velvetyne Type Foundry
     */
    if(!loaded) loaded = minipax.loadFromFile("minipax.ttf");
    if(!loaded)
        std::cerr << "ERREUR: Le fichier 'minipax.ttf' n'a pas été trouvé dans le dossier."
             << "Le programme ne pourra pas afficher de texte !" << std::endl;
    return loaded;
}

void draw_text(sf::RenderWindow& w, Point pos, int size, std::string str, sf::Color co) {
    if(loadfont()) {
        sf::Text text;
        text.setFont(minipax);
        text.setString(str);
        text.setCharacterSize(size);
        //text.setFillColor(co);
        text.setPosition(pos.x, pos.y);
        w.draw(text);
    }
}

Point wait_mouse(sf::RenderWindow& w) {
    sf::Event e;
    do w.waitEvent(e);
    while(e.type != sf::Event::MouseButtonPressed or
            e.mouseButton.button != sf::Mouse::Button::Left);
    return Point(e.mouseButton.x, e.mouseButton.y);
}

sf::Event::KeyEvent wait_keyboard(sf::RenderWindow& w) {
    sf::Event e;
    do w.waitEvent(e);
    while(e.type != sf::Event::KeyPressed);
    return e.key;
}

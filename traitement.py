lignes = []
with open("situations.csv", "r") as f:
	for ligne in f.readlines():
		if len(ligne) >= 15 + 5: lignes.append(ligne)

for index, ligne in enumerate(lignes.copy()):
	if len(ligne) < 10015: continue

	situation, infos = ligne.split(";")
	infos = infos.replace("\n", "")
	stats = {"1":0, "2":0, "3":0, "4":0, "5":0}
	for coup in infos:
		stats[coup] += 1
	copie = stats.copy()
	for coup, nbOcc in copie.items():
		stats[coup] = round(10000 * nbOcc / sum(copie.values()))
	infos = "".join([coup * nbOcc for coup, nbOcc in stats.items()])
	lignes[index] = situation + ";" + infos + "\n"

with open("situations.csv", "w") as f:
	for ligne in lignes: f.write(ligne)

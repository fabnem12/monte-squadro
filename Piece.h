#ifndef DEF_PIECE
#define DEF_PIECE

#include <vector>

class Joueur;
#include "Joueur.h"
class Plateau;
#include "Plateau.h"

class Piece
{
public:
    /** Constructeur de Piece (fait par Paul)
     * @param le pointeur du joueur propriétaire de la pièce
     * @param la "position variable" (colonne pour le joueur horizontal, ligne pour l'autre) de la pièce
     * @param la "position fixe" (ligne pour le joueur horizontal, colonne pour l'autre) de la pièce
     * @param la pièce est-elle en phase aller ou retour ?
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    Piece(Joueur* proprio, int posVariable, int posFixe, bool aller = true);

    /** Fonction accesseur de m_aller (fait par Paul)
     * @return un booléen true si dans le sens aller, false si dans le sens retour
    **/
    bool aller();

    /** Fonction renvoyant si la pièce est arrivé (faite par Fabrice)
     * @return un booléen true si la pièce est arrivée, false sinon
    **/
    bool arrive();

    /** Fonction donnant la "position variable" de la pièce (colonne pour une pièce se déplaçant horizontalement, ligne sinon) (faite par Paul)
     * @return un nombre de 0 à 6 correspondant à la "position variable" sur le Plateau
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    int positionVariable();

    /** Fonction donnant la "position fixe" de la pièce (ligne pour une pièce se déplaçant horizontalement, colonne sinon) (faite par Paul)
     * @return un nombre de 1 à 5 correspondant à la "position fixe" sur le Plateau
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    int positionFixe();

    /** Méthode changeant la "position variable" d'une pièce (faite par Paul)
     * @param la nouvelle position variable de la pièce
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    void setPosVariable(int posX);

    /** Méthode changeant le sens d'une pièce (faite par Fabrice)
    **/
    void setAller();

    /** Fonction retournant la "position variable" théorique de la pièce compte tenu de son nombre de cases de déplacement (faite par Paul)
     * @param le pointeur du plateau de jeu
     * @return la position d'arrivée théorique de la pièce (sans tenir compte des pièces mangées)
     * Complexité : O(1)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    int positionVoulueX(Plateau* plateau);

    /** Fonction renvoyant le pointeur du joueur propriétaire de la pièce (faite par Fabrice)
     *  @return le pointeur vers le propriétaire (Joueur) de la pièce
    **/
    Joueur* proprio();

    /** Retour d'une pièce à sa dernière case de départ
    **/
    void retourDepart();
private:
    std::vector<int> m_positions;
    bool m_aller;
    Joueur* m_proprio;
};

#endif // DEF_PIECE

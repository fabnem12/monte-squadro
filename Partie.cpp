#include "Partie.h"

Partie::Partie(std::unordered_map<std::string, std::vector<int>>& situationsConnues, std::string mini, bool console, bool apprentissage, bool IA1, bool IA2, bool apprentissageVolee): m_modeConsole(console), m_apprentissage(apprentissage)
{
    std::pair<int, std::vector<std::vector<std::vector<int>>>> infosPieces = mini2pieces(mini);
    std::vector<std::vector<std::vector<int>>> piecesJoueurs = infosPieces.second;

    m_idJoueur = infosPieces.first;

    m_joueurs = std::vector<Joueur*>(2);
        m_joueurs[0] = new Joueur(situationsConnues, 0, piecesJoueurs[0], IA1, apprentissageVolee);
        m_joueurs[1] = new Joueur(situationsConnues, 1, piecesJoueurs[1], IA2, apprentissageVolee);

    m_gameBoard = new Plateau(m_joueurs);

    if (!console)
        m_fenetre = new sf::RenderWindow(sf::VideoMode(1050, 700), "Squadro");
}

Partie::Partie(std::unordered_map<std::string, std::vector<int>>& situationsConnues, std::pair<int, std::vector<std::vector<std::vector<int>>>> infosPieces, bool console, bool apprentissage, bool IA1, bool IA2, bool apprentissageVolee): m_modeConsole(console), m_apprentissage(apprentissage)
{
    std::vector<std::vector<std::vector<int>>> piecesJoueurs = infosPieces.second;

    m_idJoueur = infosPieces.first;

    m_joueurs = std::vector<Joueur*>(2);
        m_joueurs[0] = new Joueur(situationsConnues, 0, piecesJoueurs[0], IA1, apprentissageVolee);
        m_joueurs[1] = new Joueur(situationsConnues, 1, piecesJoueurs[1], IA2, apprentissageVolee);

    m_gameBoard = new Plateau(m_joueurs);

    if (!console)
        m_fenetre = new sf::RenderWindow(sf::VideoMode(1050, 700), "Squadro");
}

Partie::~Partie() //Destructeur
{
    delete m_joueurs[0];
    delete m_joueurs[1];
    delete m_gameBoard;
    if (!m_modeConsole) delete m_fenetre;
}

void Partie::interaction()
{
    if (!m_apprentissage) affichePlateau();

    bool premier = true;

    while (!finPartie()) {
        if (!m_apprentissage) {
            if (!premier) affichePlateau();
            enregistrement();
        }

        int y = demandeCoup();
        if (y == -42) return; // fin de partie en fermant la fenêtre
        m_joueurs[m_idJoueur]->coup(m_gameBoard, y);

        if (!m_apprentissage) {
            finCoup();
        }
        changementJoueur();

        premier = false;
    }
    return;
}

void Partie::changementJoueur()
{
    m_idJoueur = (m_idJoueur == 0);
}

void Partie::affichePlateau()
{
    if (m_modeConsole)
        m_gameBoard->affichage(m_joueurs[m_idJoueur]);
    else {
        m_fenetre->clear(sf::Color::White);
        m_gameBoard->affichageFenetre(m_fenetre, m_joueurs[m_idJoueur]);
    }
}

bool coachEstAuto() {
    std::ifstream configCoach;
    configCoach.open("coachAuto.txt");
    std::string param;
    configCoach >> param;
    configCoach.close();

    return (param == "oui");
}

int Partie::demandeCoup()
{
    int y(-1);

    if (joueurEnCours()->estIA())
        return joueurEnCours()->IA_coup(m_gameBoard) - 1; //l'ia ne peut jouer que des coups valides
    do {
        bool coachAuto = coachEstAuto();

        if (m_joueurs[1-m_idJoueur]->estIA()) {//Partie humain contre IA, on donne à l'humain le coup que l'ia aurait joué à sa place
            std::string miniActuelle = plateau2mini(m_gameBoard, m_joueurs[m_idJoueur]);

            if (coachAuto) {
                if (estDansMap(joueurEnCours()->bdSituations(), miniActuelle)) {
                    y = coupPlusFrequent(joueurEnCours()->bdSituations()[miniActuelle]);
                } else { //Si le coup est inconnu, on tire un coup au hasard
                    std::random_device dev;
                    std::mt19937 rng(dev());
                    std::uniform_int_distribution<std::mt19937::result_type> dist(1, 5);
                    y = dist(rng);
                }
                std::cout << "Le coach joue automatiquement " << ((1-m_idJoueur) ? 6-y : y) << std::endl;
            } else {
                if (estDansMap(joueurEnCours()->bdSituations(), miniActuelle)) {
                    int coupRecommande = 6-coupPlusFrequent(joueurEnCours()->bdSituations()[miniActuelle]);
                    if (m_idJoueur) coupRecommande = 6-coupRecommande;

                    std::cout << "L'ia aurait joué le pion n°" << coupRecommande << std::endl;

                    int nbOccurences = 0;
                    for (int coup : joueurEnCours()->bdSituations()[miniActuelle])
                        nbOccurences += ((coup == coupRecommande && m_idJoueur) || (coup == 6-coupRecommande && !m_idJoueur));
                    std::cout << nbOccurences << " victoires par ce coup, sur " << joueurEnCours()->bdSituations()[miniActuelle].size() << " victoires au total." << std::endl;
                }
            }
        }

        if (m_modeConsole) {
            std::cout << "A votre tour Joueur " << m_idJoueur+1 << " !!" << std::endl;
            std::cin >> y;
            y = (1-m_idJoueur) ? 6 - y : y;
        }
        else if (!coachAuto) {
            affichePlateau();
            sf::Event e;
            do m_fenetre->waitEvent(e);
            while((e.type != sf::Event::MouseButtonPressed || e.mouseButton.button != sf::Mouse::Button::Left) && e.type != sf::Event::Closed);

            if (e.type == sf::Event::Closed) {
                m_fenetre->close();
                return -42;
            }

            Point point = Point(e.mouseButton.x, e.mouseButton.y);

            //Point point = wait_mouse(*m_fenetre); //Détection du clic sur la souris
            if (1-m_idJoueur && point.x < 700) //On ignore les clics hors du damier
                y = 7 - (point.y / 100);  //Le joueur 1 a la pièce n°1 en bas ! et les cases font 100 pixels de côté
            else if (point.x < 700)
                y = point.x / 100;
        }
    } while(!verifCoup(y));

    return y-1; //Jusqu'ici, le y était entre 1 et 5, on ramène cela à un nombre entre 0 et 4 afin de le traiter plus facilement
}

bool Partie::verifCoup(int coup)
{
    if ((coup>5 || coup<=0) || m_joueurs[m_idJoueur]->pieces()[coup-1]->arrive()) {
        return false;
    }

    return true;
}

void Partie::finCoup()
{
    affichePlateau(); //On rafraîchit l'image du plateau après le coup du joueur

    if (!m_modeConsole) {
        //Lors d'une partie IA contre IA (ou ia contre coach), on confirme à la main (en tapant sur entrée) chaque coup, pour voir ce qu'il se passe
        bool coachAuto = coachEstAuto();

        if ((m_joueurs[0]->estIA() && m_joueurs[1]->estIA()) || (m_joueurs[0]->estIA() && !m_joueurs[1]->estIA() && coachAuto) || (!m_joueurs[0]->estIA() && m_joueurs[1]->estIA() && coachAuto)) {
            if (!m_joueurs[m_idJoueur]->estIA()) return;

            sf::Event::KeyEvent evenement;
            do {
                evenement = wait_keyboard(*m_fenetre); //Détection du clavier
            } while (evenement.code != sf::Keyboard::Key::Return);
        }
    }
}

bool Partie::finPartie()
{
    int perdant = m_idJoueur;
    int gagnant = 1-perdant;

    if (m_joueurs[gagnant]->gagnant()){
        m_joueurs[gagnant]->enregistreCoups(); //seul le gagnant doit enregistrer ses coups
	      if (m_apprentissage) return true;

        if (!m_apprentissage) std::cout << "Le joueur " << gagnant+1 << " a cordialement écrasé le joueur " << perdant+1 << std::endl;
        if (!m_apprentissage) enregistrement();
        if (!m_modeConsole) {
            sf::Event e;
            do m_fenetre->waitEvent(e);
            while(e.type != sf::Event::Closed && e.type != sf::Event::KeyPressed);
            m_fenetre->close();
        }

        //Enregistrement de qui a gagné entre ia et humain lors d'une partie humain contre ia
        bool coachAuto = coachEstAuto();

        std::string nomFichierSave;
        if (m_joueurs[0]->estIA() && !m_joueurs[1]->estIA())
            nomFichierSave = (!coachAuto) ? "parties_ia1_humain2.txt" : "parties_ia1_coach2.txt";
        else if (!m_joueurs[0]->estIA() && m_joueurs[1]->estIA())
            nomFichierSave = (!coachAuto) ? "parties_humain1_ia2.txt" : "parties_coach1_ia2.txt";
        else if (m_joueurs[0]->estIA() && m_joueurs[1]->estIA() && !m_apprentissage)
            nomFichierSave = "parties_ia_vs_ia.txt";


        std::ofstream fichierLog;
        fichierLog.open(nomFichierSave, std::ios_base::app);

        fichierLog << gagnant+1 << std::endl; //Enregistre le numéro du joueur gagnant
        fichierLog.close();

        return true;
    }
    else return false;
}

void Partie::enregistrement()
{
    std::ofstream fichierConfig;
    fichierConfig.open("config.cfg");

    fichierConfig << ((m_joueurs[0]->estIA()) ? "ia" : "humain") << std::endl;
    fichierConfig << ((m_joueurs[1]->estIA()) ? "ia" : "humain") << std::endl;
    fichierConfig << (m_modeConsole ? "console" : "fenetre") << std::endl;
    fichierConfig << "volee" << std::endl;

    if (m_joueurs[0]->gagnant() || m_joueurs[1]->gagnant()) { //Un joueur a gagné, on refait un fichier de configuration qui permet de commencer à zéro au prochain lancement
        fichierConfig << "0D=5.'LMNOP" << std::endl;

        std::ofstream fichierFinParties;
        fichierFinParties.open("finParties.txt", std::ios_base::app);
        fichierFinParties << plateau2mini(m_gameBoard, m_joueurs[m_idJoueur]) << std::endl;
        fichierFinParties.close();
    } else
        fichierConfig << plateau2mini(m_gameBoard, m_joueurs[m_idJoueur]) << std::endl;

    fichierConfig.close();
}

Plateau* Partie::plateau()
{
    return m_gameBoard;
}

Joueur* Partie::joueurEnCours()
{
    return m_joueurs[m_idJoueur];
}

std::vector<Joueur*>& Partie::joueurs()
{
    return m_joueurs;
}

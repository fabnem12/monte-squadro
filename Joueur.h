#ifndef DEF_JOUEUR
#define DEF_JOUEUR

#ifdef _WIN32
#include <cstdlib>
#endif

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <random>
#include <unordered_map>
#include <chrono>

class Joueur;

#include "situationsTOstring.h"

class Partie;
#include "Partie.h"
class Plateau;
#include "Plateau.h"
class Piece;
#include "Piece.h"

int randint(int inf, int sup);

class Joueur
{
public:
    /**Constructeur (fait par Paul et Fabrice)
     * @param la référence de la unordered_map qui recense les situations connues
     * @param l'id du joueur (0 pour l'horizontal, 1 pour l'autre)
     * @param un vector<vector<int>> résumant les positions et direction des pièces du joueur, afin de les intialiser
     * @param le joueur est-il IA ?
     * @param l'IA a-t-elle le droit de faire de l'apprentissage à la volée
     * Complexité : O(nombre de pièces)
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    Joueur(std::unordered_map<std::string, std::vector<int>>& situationsConnues, int id, std::vector<std::vector<int>> positionPieces, bool IA = true, bool apprentissageVolee = false);

    ~Joueur(); // Destructeur (fait par Paul)

    /** Fonction gérant les déplacements des pièces (faite par Paul)
      * @param le pointeur du plateau
      * @param la position en y de la pièce que le joueur souhaite déplacer
      * Complexité : O(1)
      * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas !
    **/
    void coup(Plateau* plateau, int y);

    /** Fonction indiquant si le joueur a gagné (faite par Paul) + enregistrement le cas échéant des coups dans m_situationsConnues (fait par Fabrice)
     * @return un booleen : true si le joueur a gagné et false si le joueur n'a pas gagné
     * Complexité : O(1) si le joueur n'est pas gagnant, O(nombre de coups joués sinon)
     * Confiance dans l'implantation : très forte, sinon la fin de partie ne serait pas détectée et l'IA ne marcherait pas
    **/
    bool gagnant();

    /** Fonction qui enregistre les coups du joueur en fin de partie, dans la map appropriée
    **/
    void enregistreCoups();

    /** Fonction accesseur de m_id
     * @return m_id
    **/
    int id();

    /** Fonction accesseur de m_estIA
     * @return m_estIA
    **/
    bool estIA();

    /** Fonction accesseur de m_pieces
     * @return m_pieces
    **/
    std::vector<Piece*>& pieces();

    /** IA : coup à jouer (faite par Fabrice)
     * @param le pointeur du plateau de jeu
     * @return le coup voulu par l'IA
     * Complexité : temps d'exécution fixé à 3 secondes pour les parties à la volée, + O(nombre de situations connues) pour le calcul de estDansMap()
     * Confiance dans l'implantation : très forte, sinon le jeu ne marcherait pas
    **/
    int IA_coup(Plateau* plateau);

    /** Nombre de pièces arrivées (faite par Fabrice)
     * @return le nombre de pièces du joueur étant arrivées
     * Complexité : O(nombre de pièces), la suite d'opérations est répétée pour chaque pièce
     * Confiance dans l'implantation : très forte, les tests passent sans problème
    **/
    int nbPiecesArrivees();

    //Accesseur de m_situationsConnues
    std::unordered_map<std::string, std::vector<int>>& bdSituations();
private:
    int m_id;
    bool m_estIA;
    std::unordered_map<std::string, std::vector<int>>& m_situationsConnues;
    std::vector<Piece*> m_pieces;
    std::map<std::string, int> m_coups;
    bool m_apprentissageVolee;
};

//Fonction pour vérifier si une entrée existe dans une map
//Merci à Anatole Dedecker pour l'astuce
template <typename map_type>
bool estDansMap(map_type& map, typename map_type::key_type key) {
    return (map.find(key) != map.end());
}

#endif // DEF_JOUEUR
